package com.tgc.testautomation.prueba_predial;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class MenuPpal {
	public MenuPpal() {
		accederaPredial(WebbDriver.driver);
	}
	public void accederaPredial(WebDriver driver) {
		switch (params.test) {
		case "RE_Predial":
			System.out.println("**************************************************\n");
			System.out.println("Accediendo a \"Consulta de predios\"\n");
			try {
				driver.findElement(By.xpath("//a[text()='Menú de predial']")).click();
				driver.findElement(By.xpath("//a[contains(text(), 'Consulta de predios')]")).click();
			} catch (NoSuchElementException e) {
				System.out.println("Error en el menú principal.");
				e.printStackTrace();
			}
			break;
		case "":
			System.out.println("**************************************************\n");
			System.out.println("Accediendo a \"Contabilidad y Cajas\"\n");
			
			break;
		default:
			break;
		}
		
		System.out.println("Listo\n");
	}
}