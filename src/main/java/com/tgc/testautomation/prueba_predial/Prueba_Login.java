package com.tgc.testautomation.prueba_predial;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Prueba_Login {

	WebDriver driver;
	int i;
	Double total;
	
	
	public Prueba_Login() {
		
		if(params.user != null && params.password != null && params.webnav != null) {
			new WebbDriver();
			driver = WebbDriver.driver;
			pruebaLogin();
			}
	}
	
	
	public void pruebaLogin() {
		i = 1;
		while(true) {
			new login(WebbDriver.driver);
			acercade();
			administracioncuenta();
			cerrarSesion();
		}
		
	}
	
	public void acercade() {
		driver.findElement(By.xpath("//a[text()='Acerca de Recaudador']")).click();
		params.randomwait();
		driver.findElement(By.xpath("//a[@href='#' and @title='Cerrar']")).click();
		params.randomwait();
	}
	
	
	
	public void administracioncuenta() {
		driver.findElement(By.xpath("//a[@onclick='return false' and text()='" + params.user + "']")).click();
		params.randomwait();
		driver.findElement(By.xpath("//td[text()='Administración de cuenta']")).click();
		
		new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Cambiar']")));
		driver.findElement(By.xpath("//a[text()='Pregunta secreta']")).click();
		params.randomwait();
		driver.findElement(By.xpath("//a[text()='Contraseña']")).click();
		params.randomwait();
		acercade();
		driver.findElement(By.xpath("//button[text()='Cancelar']")).click();
		params.randomwait();

	}
	
	public void cerrarSesion() {
		driver.findElement(By.xpath("//a[@onclick='return false' and text()='" + params.user + "']")).click();
		driver.findElement(By.xpath("//td[text()='Cerrar sesión']")).click();
		new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Cerrar sesión']")));
		driver.findElement(By.xpath("//button[text()='Cerrar sesión']")).click();
		params.randomwait();
	}
	
	
	
	@SuppressWarnings("unused")
	private void BackToMainMenu() {
		try {
			driver.findElement(By.xpath("//img[contains(@title,'Volver al men')]")).click();

		} catch (Exception e) {
			System.out.println("Problema al regresar al menú");
			e.printStackTrace();
			BackToHome();
		}
	}
	
	
	
	private void BackToHome() {
		System.out.println("Menú principal");
		driver.get(params.url);
		new WebDriverWait(WebbDriver.driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Seleccione el módulo al que desea ingresar.']")));
	}
}
