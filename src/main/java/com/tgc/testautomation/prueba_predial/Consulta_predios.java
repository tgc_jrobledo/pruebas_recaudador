package com.tgc.testautomation.prueba_predial;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



//Método que hace todo el ciclo de búsqueda de una clave catastral, regresa al buscador y continúa con los siguientes.
public class Consulta_predios {		
	WebDriver driver = WebbDriver.driver;

	public Consulta_predios() {
		//guarda la url del buscador de predial, para regresar a él en caso de un problema
		params.setPredial_url(driver.getCurrentUrl());
		
		System.out.println("**************************************************\n");
		System.out.println("Ciclo de búsqueda de predios\n");
		
		
		for (String clave : params.Claves_catastrales) {
			System.out.println("----------------------------------------\n");
			System.out.println(clave + "\n");
			try {
				Buscar_clave(clave);
			} catch (org.openqa.selenium.TimeoutException e) {
				System.out.println(e.getMessage());
				System.out.println("Excepción encontrada, se alcanzó el límite de tiempo de espera para cargar la página"
						+ "\n Skippeando clave...");
				params.removeClavePrediofromCSV(clave);
				BackToHome();
				continue;
			}
			Detalles_propietario();
			if (!BackToSearch()) {
				BackToMainMenu();
			}
		}
	}
	
	
	
	
	//Se busca la clave catastral obtenida de la lista del CSV en el buscador
	private void Buscar_clave(String clave) {
		
		driver.findElement(By.xpath("//label[text()='Clave catastral']//following::input")).clear();
		driver.findElement(By.xpath("//label[text()='Clave catastral']//following::input")).sendKeys(clave);

		params.randomwait();

		driver.findElement(By.xpath("//button[text()='Buscar']")).click();

		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='" + clave + "']")));
		
		if (driver.findElement(By.xpath("//span[text()='" + clave + "']//ancestor::tr[1][contains(@class, 'Selected')]"))!=null) {
			driver.findElement(By.xpath("//span[text()='Siguiente']//ancestor::button")).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Persona')]")));

		}
	}
	
	private void Detalles_propietario() {
		
		System.out.println("Navegando Pestañas...");
		
		
		//navegar por las pestañas de 'detalles del propietario'
		System.out.print("Detalles de propietario: \t");
		String xpath_detallesProp = "//a[contains(text(),'Persona')]";
		driver.findElement(By.xpath(xpath_detallesProp + "//following::a")).click();
		params.randomwait();

		driver.findElement(By.xpath(xpath_detallesProp + "//following::a//following::a")).click();
		params.randomwait();

		driver.findElement(By.xpath(xpath_detallesProp + "//following::a//following::a//following::a")).click();
		params.randomwait();
		System.out.println("✓");
		
		
		//Abrir pestañas de Valuaciones
		System.out.print("Valuaciones:\t\t\t");
		driver.findElement(By.xpath("//button[text()='Ver Valuaciones']")).click();
		params.randomwait();

		String xpath_Valuacion = "//a[contains(text(),'Terrenos')]";
		
		driver.findElement(By.xpath(xpath_Valuacion + "//following::a")).click();
		params.randomwait();

		driver.findElement(By.xpath(xpath_Valuacion + "//following::a//following::a")).click();
		params.randomwait();

		driver.findElement(By.xpath("//button[text()='Cerrar']")).click();
		params.randomwait();
		System.out.println("✓");

		
		//Ver historial de pagos
		System.out.print("Historial de pagos:\t\t");

		driver.findElement(By.xpath("//button[text()='Historial de pagos']")).click();
		params.randomwait();

		driver.findElement(By.xpath("//table[@style='position: relative;']//child::a[@title='Cerrar']")).click();
		params.randomwait();
		System.out.println("✓");

		
		
		//Pestañas "detalles del predio"
		System.out.print("Obligaciones y Documentos:\t");

		String xpath_pDetalles = "//a[contains(text(),'Datos del predio')]";
		driver.findElement(By.xpath(xpath_pDetalles + "//following::a")).click();
		params.randomwait();

		driver.findElement(By.xpath(xpath_pDetalles + "//following::a//following::a")).click();
		params.randomwait();
		System.out.println("✓");

		System.out.println("\nListo!\n");
	}

	
	private boolean BackToSearch() {
		driver.findElement(By.xpath("//span[text()='Anterior']//parent::button")).click();;
		params.randomwait();

		
		if (driver.findElement(By.xpath("//div[@title='Consulta de predios']"))!=null) {
			return true;
		} else return false;
	}
	
	
	private void BackToMainMenu() {
		driver.findElement(By.xpath("//img[contains(@title,'Volver al men')]")).click();
		new MenuPpal().accederaPredial(driver);
	}
	
	private void BackToHome() {
		System.out.println("Regresando al menú de búsqueda");
		driver.get(params.predial_url);
		new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@title='Consulta de predios']")));		
	}
	
	
}
