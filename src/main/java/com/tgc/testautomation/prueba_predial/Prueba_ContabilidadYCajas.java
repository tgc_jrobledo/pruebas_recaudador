package com.tgc.testautomation.prueba_predial;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;




public class Prueba_ContabilidadYCajas {
	
	WebDriver driver;
	
	int i;
	Double total;
	
	
	public Prueba_ContabilidadYCajas() {
		if(params.user != null && params.password != null && params.webnav != null) {
			new WebbDriver();
			driver = WebbDriver.driver;
			CicloDeCierre();
		}
	}
	
	
	
	
	
	
	
	
	
	
	private void CicloDeCierre() {
		i=1;
		new login(WebbDriver.driver);
		while(i<100) {
			System.out.println("**************************************************\n");
			System.out.println("Ciclo de Cierres, intento #" + i);
			
			DBconn.insertCorte();
			
			
			if (!RealizarCorte()) {
				System.out.println("Problema al realizar el corte - paso 1");
				DBconn.deleteCorte();
				BackToHome();
				i++;
				continue;
			} 
			
			
			
			BackToMainMenu();
			
			
			if (!recepcionCortes()) {
				System.out.println("Error en recepción de corte - paso 2");
				DBconn.setCorteterminado();
				BackToHome();
				i++;
				continue;
			}
			
			
			
			BackToMainMenu();
			
			
			if(!realizarCierre()){
				System.out.println("Error en realización de cierre - paso 3, intentando una segunda vez");
				BackToHome();
				if (!realizarCierre()) {
					DBconn.setCorteterminado();
					BackToHome();
					i++;
					continue;
				}
			}
			
			
			
			BackToMainMenu();
		}
		System.out.println("Prueba terminada, Revisa ADF  Performance monitor.");
	}
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	private boolean RealizarCorte() {
		System.out.println("**************************************************\n");
		System.out.println("Realizando corte de caja...\n");
		
		try {
			driver.findElement(By.xpath("//a[text()='Menú de Cajas']")).click();
			driver.findElement(By.xpath("//a[contains(@href, 'trcoRealizacionDeCorteMain')]")).click();
			new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[text()='Instrumento de pago']")));
		} catch (Exception e) {
			System.out.println("Error al pasar del menú");
			return false;
		}
		
		
		
		
		try {
			Select sel_InstrumentoDePago = new Select(driver.findElement(By.xpath("//select")));
			sel_InstrumentoDePago.selectByIndex(1);
			List<WebElement>  l = driver.findElements(By.xpath("//input[not(@type='hidden')]"));
			
			for (WebElement inputDinero : l) {
				inputDinero.clear();
				inputDinero.sendKeys(String.valueOf(randomNumber()));
			}
		} catch (Exception e) {
			System.out.println("Error llenando campos de moneda/billetes");
			return false;
		}
		
		
		
		
		
		
		
		try {
			driver.findElement(By.xpath("//button[text()='Recalcular']")).click();
			new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(text(), 'Total=')]")));
			params.randomwait();
		} catch (Exception e) {
			System.out.println("Error al dar click en recalcular");
			return false;
		}
		
		
		
		String TMP = driver.findElement(By.xpath("//td[contains(text(), 'Total=')]")).getText().replace("Total= ", "");
		total = Double.parseDouble(TMP);
		System.out.println("Total: " + total);
		
		
		
		
		try {
			driver.findElement(By.xpath("//button[text()='Realizar corte']")).click();
			params.randomwait();
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			System.out.println("Error al Dar click en realizar Corte");
			return false;
		}
		


		
		
		
		System.out.println("Corte terminado, regresando al menú principal");
		params.randomwait();
		
		
		
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	private boolean recepcionCortes() {
		System.out.println("**************************************************\n");
		System.out.println("Recepción de Corte...\n");
		
		
		try {
			driver.findElement(By.xpath("//a[text()='Menú de Administración de Cajas']")).click();
			driver.findElement(By.xpath("//a[contains(@href, 'consultasRecepcionBusq')]")).click();
			new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Buscar por: ']")));
		} catch (Exception e) {
			System.out.println("Problema al tratar de pasar del menú a recepción del corte");
			return false;
		}
		
		
		
		try {
			driver.findElement(By.xpath("//label[text()='Serie: ']//following::input")).sendKeys("2019");
			driver.findElement(By.xpath("//label[text()='Identificador: ']//following::input"))
			.sendKeys(String.valueOf(params.ID_Corte));
			driver.findElement(By.xpath("//button[@id='buscar']")).click();
		} catch (Exception e) {
			System.out.println("Problema con el buscador de corte");
			return false;
		}
		
		params.randomwait();
		
		try {
			driver.findElement(By.xpath("//button[text()='Recibir corte']")).click();
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			System.out.println("Problema al momento de pusharle a recibir corte");
			return false;
		}
		
		System.out.println("Recepción de corte finalizada con éxito");
		return true;
	}
	
	
	
	
	
	
	
	
	
	private boolean realizarCierre() {
		System.out.println("**************************************************\n");
		System.out.println("Realizar el cierre...\n");
		
		try {
			driver.findElement(By.xpath("//a[text()='Menú de Administración de Cajas']")).click();
			driver.findElement(By.xpath("//a[contains(@href, 'trcoRealizaCierreMain')]")).click();
			new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated
					(By.xpath("//option[@selected and contains(text(), 'CHEQUE')]//parent::select")));
		} catch (Exception e) {
			System.out.println("Error al intentar pasar del menú principal a la pantalla de realizarCierre");
			return false;
		}
		
		
		
		
		
		try {
			Select instrumentoPago = new Select(driver.findElement(By.xpath("//option[@selected and contains(text(), 'CHEQUE')]//parent::select")));
			instrumentoPago.selectByValue("1");
			driver.findElement(By.xpath("//input[@maxlength='15']")).sendKeys(String.valueOf(total));;
			driver.findElement(By.xpath("//textarea")).sendKeys("Prueba automatizada selenium para Performance Monitor - Intento #" + i++);
			params.randomwait();
			driver.findElement(By.xpath("//button[text()='Agregar']")).click();
			new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Realizar Cierre']")));
		} catch (Exception e) {
			System.out.println("Problema al momento de llenar los datos del cierre");
			return false;
		}
		
		try {
			driver.findElement(By.xpath("//button[text()='Realizar Cierre']")).click();
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			System.out.println("Pedo al ya realizar el cierre alv");
			return false;
		}
		
		
		
		
		System.out.println("Cierre Realizado correctamente.");
		params.randomwait();
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	private void BackToMainMenu() {
		try {
			driver.findElement(By.xpath("//img[contains(@title,'Volver al men')]")).click();

		} catch (Exception e) {
			System.out.println("Problema al regresar al menú");
			e.printStackTrace();
			BackToHome();
		}
	}
	private void BackToHome() {
		System.out.println("Menú principal");
		driver.get(params.url);
		new WebDriverWait(WebbDriver.driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Seleccione el módulo al que desea ingresar.']")));
	}
	private int randomNumber(){
		int valor = (int) (Math.random()*((9-1)+1));
		return valor;
	}
}