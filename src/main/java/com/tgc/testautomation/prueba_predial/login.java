package com.tgc.testautomation.prueba_predial;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class login {
	public login(WebDriver driver) {
		System.out.println("**************************************************\n");
		
		try {
		driver.get(params.url);
		System.out.println("Iniciando sesión en " + driver.getCurrentUrl());

		
		
		params.randomwait();
		driver.findElement(By.xpath("//input[@name='txtUsuario']")).sendKeys(params.user);
		driver.findElement(By.xpath("//input[@name='txtPass']")).sendKeys(params.password);
		driver.findElement(By.xpath("//button[contains(text(), 'Iniciar ses')]")).click();
		WebDriverWait wait = new WebDriverWait(driver,10);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@onclick='return false' and text()='" + params.user + "']")));
		
		
		if (driver.findElement(By.xpath("//a[@onclick='return false' and text()='" + params.user + "']")) != null) {
			System.out.println("Login Exitoso\n");
		}
		
		
		} catch (Exception e) {
			if (driver.findElement(By.xpath("//a[@onclick='return false' and text()='" + params.user + "']")) != null) {
				System.out.println("Sesión aún activa, no es necesario el login de nuevo.");
			}
			System.out.println("Error en el login");
			e.printStackTrace();
		}
	}
}
