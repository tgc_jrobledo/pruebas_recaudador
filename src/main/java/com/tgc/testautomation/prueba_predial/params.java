package com.tgc.testautomation.prueba_predial;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;


public class params {
	static String user, password, webnav, url, test, fecha;
	static ArrayList<String> Claves_catastrales;
	static String csvFile="predios.csv";
	static String ParamsFile = "settings.xml";
	static String predial_url;
	static Integer ID_Corte;
	

	public params() {
		System.out.println("**************************************************\n");
		System.out.println("Cargando Parámetros...\n");
		getParamsfromXML();
		
	}
	
	
	
	public void getParamsfromXML() {
        try {

		SAXReader reader = new SAXReader();
		Document document = reader.read(ParamsFile);
		Element root = document.getRootElement();
		
		
		
		webnav = root.elementText("Driver");
		user = root.elementText("RECA_user");
		password = root.elementText("RECA_psswd");
		url = root.elementText("URL");
		test = root.elementText("Test");
		ID_Corte =Integer.parseInt(root.elementText("ID_Corte"));
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		fecha =format.format(LocalDate.now());
		
		System.out.println("Navegador: " + webnav + "\nUsuario: " + user + "\nUrl: " +  url + "\nTipo de prueba: " +  test);
		
		} catch (DocumentException e) {
			
			e.printStackTrace();
		}
        
        
        switch (test) {
		case "RE_Predial":
			getPrediosfromCSV();
			new Prueba_predial();
			break;
		case "RE_ContabilidadYCajas":
			System.out.println("Prueba de Ciclo de Cortes y Cierres");
			new DBconn();
			ID_Corte = DBconn.getIDCierre_BD();
			new Prueba_ContabilidadYCajas();
			break;
		case "RE_Login":
			System.out.println("Prueba de Login");
			new Prueba_Login();
			break;
		default:
			break;
		}
        
	}
	
	public void getPrediosfromCSV() {
		String line;
		Claves_catastrales =new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			while ((line = br.readLine()) != null) {

                Claves_catastrales.add(line);
            }
			System.out.println("Cargadas " + Claves_catastrales.size() + " claves del archivo " + csvFile);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	static void removeClavePrediofromCSV(String clave) {
		String line;
		ArrayList<String> quitarclave = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			while ((line = br.readLine()) != null) {
				if (!line.equals(clave)) {
					quitarclave.add(line);
				}
            }
			BufferedWriter out = new BufferedWriter(new FileWriter(csvFile));
			for (String clavve : quitarclave) {
				out.append(clavve + "\n");
			}
			out.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public static void setPredial_url(String predial_url) {
		params.predial_url = predial_url;
	}
	
	
	
	
	
	static void randomwait() {
		int wait = (int) ((Math.random()*((4-2)+1))+2);

		try {
			Thread.sleep(wait*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	static int updateID_Corte() {
		ID_Corte++;
		try {

			SAXReader reader = new SAXReader();
			Document document = reader.read(ParamsFile);
			document.getRootElement().element("ID_Corte").setText(ID_Corte.toString());			
			
			
			OutputFormat format = OutputFormat.createPrettyPrint();
			XMLWriter writer;
	        writer = new XMLWriter(new FileOutputStream(new File(ParamsFile)), format);
	        writer.write(document);
	        writer.close();
		}
		catch (Exception e) {
			System.out.println("Error actualizando valor de ID_Corte");
		}
			return ID_Corte;
	}
	
}
