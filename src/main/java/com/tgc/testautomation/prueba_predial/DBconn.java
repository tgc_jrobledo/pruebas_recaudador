package com.tgc.testautomation.prueba_predial;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DBconn {
	
	static String 	user = "DEV_MR0023", 
					pass ="dev_mr0023",
					url = "jdbc:oracle:thin:@odb12.tgc.mx:1521/rec.tgc.mx";
	static Connection conn;
	 public DBconn() {
		 try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		 
	 }
	 
	 
	 
	 static int getIDCierre_BD() {
		 int value = 0;
		 
		 try {
			 
			 conn = DriverManager.getConnection(url, user, pass);
			 
			 if(conn!=null){ //Si la conexión fue exitosa imprimimos un mensaje
				 System.out.println(url + " - Conexión establecida.");
			 }
			 
			 
			 Statement stmt = conn.createStatement();
			 String consulta = "Select IDENTIFICADOR from CC_CORTES order by serie desc, identificador desc fetch first row only";
			 
			 ResultSet rs=stmt.executeQuery(consulta);
			 
			 while (rs.next()) {
				 
				 value = rs.getInt(1);
				 }
			 System.out.println("Se obtuvo el último identificador de la base de datos - " + value);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		 return value;
	 }
	 
	 
	 
	 
	 static void insertCorte() {
		 try {
			 
			 System.out.println("**************************************************\n");
			 System.out.println("Insertando corte a la base de datos " + user + ":\n");
			 
			 
			 
			 if(conn!=null){//Si la conexión fue exitosa imprimimos un mensaje
				 System.out.println(url + " - Conexión establecida.");
				 }
			 
			 Statement stmt = conn.createStatement();
			 String insertQuery = "Insert into CC_CORTES values (2019, " + params.updateID_Corte() + ", SYSDATE, null, SYSDATE, 'AC', 'CA', "
					 			+ "0, 0, 0, 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null, null, null, null, 2019, 8)";
			 System.out.println(insertQuery);
			 stmt.executeUpdate(insertQuery);
			 
			 System.out.println("Se agregó un Corte a la base de datos");
			 
			 stmt.close();
			 
			 
			 
			} catch (SQLException e) {
				System.out.println("Error de SQL");
				e.printStackTrace();
			}
	 }
	 
	 
	 static void deleteCorte() {
		 try {
			 
			 System.out.println("**************************************************\n");
			 System.out.println("Borrando registro de la base de datos " + user + ":\n");
			 
			 
			 
			 if(conn!=null){//Si la conexión fue exitosa imprimimos un mensaje
				 System.out.println(url + " - Conexión activa.");
				 }
			 
			 Statement stmt = conn.createStatement();
			 String insertQuery = "Delete from CC_Cortes where SERIE = 2019 and IDENTIFICADOR = " + params.ID_Corte;
			 System.out.println(insertQuery);
			 stmt.executeUpdate(insertQuery);
			 
			 System.out.println("Se borró el registro " + params.ID_Corte + " de la base de datos.");
			 
			 stmt.close();
			 
			 
			 
			} catch (SQLException e) {
				System.out.println("Error de SQL");
				e.printStackTrace();
			}
	 }
	 
	 //este método se encarga de actualizar el último registro creado en la DB, 
	 //en caso de que el ciclo automático haya fallado después de cerrar el corte de billetes/monedas
	 //porque en este punto ya no se puede Borrar por llaves foráneas.
	 static void setCorteterminado() {
		 try {
			 
			 System.out.println("**************************************************\n");
			 System.out.println("Actualizando Estatus de Corte por Error en la aplicación:\n");
			 
			 Class.forName("oracle.jdbc.driver.OracleDriver");
			 Connection conn = DriverManager.getConnection(url, user, pass);
			 
			 if(conn!=null){//Si la conexión fue exitosa imprimimos un mensaje
				 System.out.println(url + " - Conexión establecida.");
				 }
			 
			 Statement stmt = conn.createStatement();
			 String insertQuery = "Update CC_CORTES set Estatus = 'TE' where serie = 2019 and identificador = " + params.ID_Corte;
			 stmt.executeUpdate(insertQuery);
			 
			 System.out.println("Se actualizó el estatus.\n");
			 
			 stmt.close();
			 conn.close();
			 
			 
			} catch (ClassNotFoundException e) {
				System.out.println("No se encontró la clase para conectar a la base de datos. Revisa "
						+ "las librerías.");
				e.printStackTrace();
			} catch (SQLException e) {
				System.out.println("Error de SQL");
				e.printStackTrace();
			}
	 }

	 
	 
	 
	 
	 
	 
}
