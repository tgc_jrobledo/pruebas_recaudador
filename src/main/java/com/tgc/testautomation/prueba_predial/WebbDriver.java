package com.tgc.testautomation.prueba_predial;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

public class WebbDriver {
	
	static WebDriver driver;
	
	public WebbDriver() {
		System.out.println("**************************************************\n");

		System.out.println("Iniciando WebDriver: ");
		switch (params.webnav) {
		
		case "CHROME":
			ChromeDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			break;
		case "FIREFOX":
			FirefoxDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			break;
		case "IEXPLORER":
			InternetExplorerDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
			break;
		default:
			ChromeDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			break;
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		
		System.out.println("WebDriver listo\n");
	}

}
